import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import config from 'config/index';
import Toolkit from './Toolkit';

const API = config.api;
const deviceEventEmitter = Toolkit.getNotificationsEmitter();
const Http = axios.create({
  baseURL: API,
});

Http.interceptors.request.use(async configuration => {
  console.log(configuration);
  const tokenValueFromStorage = await AsyncStorage.getItem('token');
  if (tokenValueFromStorage) {
    configuration.headers.Authorization = `Bearer ${tokenValueFromStorage}`;
  }
  return configuration;
});

// Add a response interceptor
Http.interceptors.response.use(
  res => {
    if (res.data.message) {
      // Toast.info(res.data.message, 5); <-- TODO: change to message provider
    }
    return res;
  },
  err => {
    if (err.response && err.response.data.error) {
      if (
        err.response.data.error.substring(0, 6) === 'TOKEN_' ||
        err.response.data.error.includes('Token expirado')
      ) {
        // Toast.fail('Sesión terminada. Por favor inicie sesión nuevamente', 5); <-- TODO: change to message provider
        deviceEventEmitter.emit('tokenError', {});
      } else {
        // Toast.fail(err.response.data.error, 5); <-- TODO: change to message provider
      }
    } else if (!err.response || !err.response.data.validation) {
      // Toast.fail(err.message, 5); <-- TODO: change to message provider
    }
    if (err.message && err.message.includes('Network Error')) {
      // Toast.fail('Error de conexión', 5); <-- TODO: change to message provider
    }
    return Promise.reject(err);
  },
);

export default Http;
