import Http from './Http';

const endpoint = 'auth';

export default {
  /**
   * Check token
   * @return {Http} GET to /auth/check
   */
  check() {
    return Http.get(`${endpoint}/check`);
  },
};
