import { REACT_APP_API_URL } from '@env';

const config = {
  apiUrl: REACT_APP_API_URL,
};

export default config;
