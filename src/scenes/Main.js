import React from 'react';
import { Button, Text, View } from 'react-native';

const fileName = 'thereactnativebook-sample.pdf';
const Main = ({ navigation }) => {
  const source = {
    uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf',
    cache: true,
  };
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>

      <Button
        title="Go to Pdf"
        onPress={() => navigation.navigate('Pdf', { fileName, source })}
      />
    </View>
  );
};

export default Main;
