# mobile.base

Base project for Digheontech React Native mobile apps

## Getting Started

Please make sure you have follow all the steps describes on [React Native setting up guide](https://reactnative.dev/docs/environment-setup)

## Start the Project

- [x] Clone this repo `git clone https://gitlab.com/digheontech/mobile.base.git`
- [x] Rename the folder to the name of your project
- [x] Change the remote repo with the following command:

  ```
  git remote set-url origin https://gitlab.com/digheontech/name-of-your-project.git
  ```

  Please replace `https://gitlab.com/digheontech/name-of-your-project.git` with your git repo url

- [x] Rename the app: `npx react-native-rename "New App Name" -b com.digheontech.newappname` (this process can take a few minutes)
- [x] Commit and push the changes to the new repo:

  ```
  git add .
  git commit -m 'changed new repo url and changed app name'
  git push origin main
  ```

- [x] Open your project and run the following command: `yarn` to install all dependencies
- [x] If you are using macOS X please run the following command: `npx pod-install`
- [x] Open your project root folder and create a .env file
- [x] Open the .env.example file copy the content and pase it on the new .env file
- [x] Setup the .env file replacing or adding the values
- [x] Run your project with the following commands:

  ```
  yarn ios // (for iOS development)
  yarn android // (for Android development)
  ```
